# Ansible Role: PostgreSQL

Installs and configures PostgreSQL server on Debian/Ubuntu servers.

# Requirements

Before starting the role, make sure you have collections installed:
```
ansible-galaxy collection install community.postgresql
ansible-galaxy collection install ansible.posix
```

No special requirements; note that this role requires root access, so either run it in a playbook with a global `become: yes`, or invoke the role in your playbook like:
```
- hosts: database
  roles:
    - role: ansible_role_postgresql
      become: yes
```